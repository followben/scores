defmodule Scores.Repo.Migrations.ConstrainPoints do
  use Ecto.Migration

  def change do
    create constraint(:users, :points_range, check: "points>=0 and points<=100")
  end
end
