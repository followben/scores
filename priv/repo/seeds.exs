# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Scores.Repo.insert!(%Scores.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Scores.Repo
alias Scores.Account.User

Enum.each(1..100, fn i -> Repo.insert! %User{} end)
