defmodule Scores.AccountTest do
  use Scores.DataCase

  alias Scores.Account
  alias Scores.Repo

  describe "users" do
    alias Scores.Account.User

    test "list_users_with_points_exceeding/2 returns 2 users with points greater than the supplied value" do
      Enum.each(0..5, fn i -> Repo.insert! %User{points: i} end)
      assert [user1, user2] = Account.list_users_with_points_exceeding(0)
      assert user1.points > 0 and user2.points > 0
      assert [user3] = Account.list_users_with_points_exceeding(4)
      assert user3.points == 5
    end

    test "list_users_with_points_exceeding/2 returns more than 2 users if requested" do
      Enum.each(0..5, fn i -> Repo.insert! %User{points: i} end)
      assert length(Account.list_users_with_points_exceeding(0, 3)) == 3
    end

    test "list_users_with_points_exceeding/2 returns an empty list if no matching users" do
      Repo.insert! %User{points: 10}
      assert length(Account.list_users_with_points_exceeding(11)) == 0
    end

    test "update_all_users_with_random_points/0 recalculates user points with the supplied function" do
      Enum.each(1..5, fn _ -> Repo.insert! %User{} end)
      Account.update_all_users_with_random_points()
      assert length(Repo.all(from u in User, where: u.points > 0)) == 5
    end

  end
end
