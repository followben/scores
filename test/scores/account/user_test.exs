defmodule Scores.UserTest do
  use Scores.DataCase

describe "user" do
    alias Scores.Account.User

    test "changeset is invalid if points exceed 100" do
      changeset = User.changeset(%User{}, %{points: 101})
      refute changeset.valid?
    end

    test "changeset is invalid if points are less than 0" do
      changeset = User.changeset(%User{}, %{points: -1})
      refute changeset.valid?
    end

    test "points default to 0" do
      user = %User{}
      assert user.points == 0
    end
  end
end
