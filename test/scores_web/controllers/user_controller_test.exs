defmodule ScoresWeb.UserControllerTest do
  use ScoresWeb.ConnCase

  alias Scores.Repo
  alias Scores.Account.User

  def fixture(:users) do
    Enum.each(1..100, fn i -> Repo.insert! %User{points: i} end)
  end

  setup %{conn: conn} do
    fixture(:users)
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do

    test "returns up to 2 users", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index))
      assert %{"users" => users} = json_response(conn, 200)
      assert length(users) <= 2
      assert [%{ "id" => id, "points" => points } | _users] = users
    end

    test "returns timestamp as nil or iso8601-compliant", %{conn: conn} do
      get(conn, Routes.user_path(conn, :index))
      conn = get(conn, Routes.user_path(conn, :index))
      assert %{"timestamp" => timestamp} = json_response(conn, 200)
      if timestamp do
        assert {:ok, _, _} = DateTime.from_iso8601(timestamp)
      end
    end

  end

end
