# Scores

Scores is an example Elixir application that uses [Phoenix](https://www.phoenixframework.org) to spawn a [GenServer implementation](https://gitlab.com/followben/scores/-/blob/master/lib/scores/server.ex) that periodically updates user scores in a database and returns them via an api endpoint, along with the time the endpoint was last accessed.

You can see the endpoint in action by running the following in your terminal:

```
% curl https://scores.gigalixirapp.com/
{"timestamp":"2020-09-28T03:02:10.496Z","users":[{"id":30,"points":58},{"id":28,"points":78}]}
```

## Installation

### Prerequisites
  * Elixir 1.10 and Erlang 23 or above [installed](https://elixir-lang.org/install.html)
  * PostgreSQL [installed](https://wiki.postgresql.org/wiki/Detailed_installation_guides)
  * Clone with `git clone https://gitlab.com/followben/scores.git` 

### Setup
  * Install dependencies with `mix deps.get`
  * If necessary, modify database connection info in config/dev.exs
  * Create and migrate the database with `mix ecto.setup`
  * Start the endpoint with `mix phx.server`
  * Run `curl http://localhost:3000` and observe the output

## Running Tests

Scores has model (unit), context and controller tests. Run them with `mix test`.