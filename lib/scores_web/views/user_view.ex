defmodule ScoresWeb.UserView do
  use ScoresWeb, :view

  alias ScoresWeb.UserView

  def render("index.json", %{users: users, timestamp: timestamp}) do
    timestamp = unless !timestamp, do: timestamp |> DateTime.truncate(:millisecond)
    %{users: render_many(users, UserView, "user.json"), timestamp: timestamp}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      points: user.points}
  end
end
