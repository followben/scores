defmodule ScoresWeb.UserController do
  use ScoresWeb, :controller

  alias Scores.Server

  action_fallback ScoresWeb.FallbackController

  def index(conn, _params) do
    result = Server.latest_scores()
    render(conn, "index.json", result)
  end
end
