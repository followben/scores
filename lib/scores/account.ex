defmodule Scores.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.Query, warn: false

  alias Scores.Repo
  alias Scores.Account.User

  @doc """
  Returns a list of users that have points greater than the value supplied.

  Supply a second argument to limit the number of users returned. Defaults to 2.

  ## Examples

      iex> list_users_with_points_exceeding(30)
      [%User{}, %User{}]

      iex> list_users_with_points_exceeding(20, 3)
      [%User{}, %User{}, %User{}]

  """
  @spec list_users_with_points_exceeding(integer, integer) :: list(User)
  def list_users_with_points_exceeding(min_points, limit \\ 2) do
    query = from u in User, where: u.points > type(^min_points, :integer), limit: type(^limit, :integer)
    Repo.all(query)
  end

  @doc """
  Transactionally updates all users in the database, generating a new random points value for each.

  ## Examples

      iex> update_all_users_with_random_points()

  """
  @spec update_all_users_with_random_points() :: any
  def update_all_users_with_random_points() do
    # setting updated_at manually as update_all doesn't do so
    from(u in User, update: [set: [points: fragment("round(random() * 100)"), updated_at: fragment("current_timestamp")]])
    |> Repo.update_all([])
  end

end
