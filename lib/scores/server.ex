defmodule Scores.Server do
  use GenServer

  alias Scores.Account

  @interval 60_000

  defmodule State do
    @enforce_keys [:max_number, :timestamp]
    defstruct [:max_number, :timestamp]

    @type t :: %__MODULE__{
      max_number: integer,
      timestamp: DateTime | nil
    }
  end

  # Client API

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(_args) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @doc """
  Returns a map containing a list of users and a UTC timestamp.

  The list contains up to two users that currently have a points score greater than the server's latest value of max_number.

  On first invocation, timestamp is nil. Thereafter it reflects the last time scores were looked up.

  ## Examples

      iex> latest_scores()
      %{users: [%User{}, %User{}], timestamp: nil}

      iex> latest_scores()
      %{users: [%User{}, %User{}], timestamp: ~U[2020-09-28T13:26:08.868569Z]}

  """
  @spec latest_scores :: %{users: list(User), timestamp: DateTime.t() | nil}
  def latest_scores() do
    GenServer.call(__MODULE__, {:lookup})
  end

  # Server API

  @spec init(:ok) :: {:ok, Scores.Server.State.t()}
  def init(:ok) do
    Process.send_after(self(), :work, @interval)
    {:ok, %State{max_number: random_score(), timestamp: nil}}
  end

  def handle_info(:work, %State{} = state) do
    Account.update_all_users_with_random_points()
    Process.send_after(self(), :work, @interval)
    {:noreply, %{state | max_number: random_score()}}
  end

  def handle_call({:lookup}, _, %State{} = state) do
    users = Account.list_users_with_points_exceeding(state.max_number)
    { :reply, %{users: users, timestamp: state.timestamp}, %{state | timestamp: DateTime.utc_now()} }
  end

  defp random_score() do
    Enum.random(0..100)
  end

end
