defmodule Scores.Account.User do
  use Ecto.Schema
  import Ecto.Changeset
  @timestamps_opts [type: :utc_datetime, usec: false]

  schema "users" do
    field :points, :integer, default: 0

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_required([:points])
    |> validate_inclusion(:points, 0..100)
    |> check_constraint(
        :points,
        name: :points_range,
        message: "points must be between 0 and 100 (inclusive)"
      )
  end
end
